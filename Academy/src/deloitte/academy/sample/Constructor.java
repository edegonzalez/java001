package deloitte.academy.sample;

public class Constructor {
	private int id;
	private String nombre;

	public Constructor() {
		this.id = 1;
		this.nombre  = "Eden Gonzalez";
	}
	

	public Constructor(int idNuevo, String nombreNuevo) {
		super();
		this.id = idNuevo;
		this.nombre = nombreNuevo;
	}

	/**
	 * 
	 * @param nuevoValor valor de tipo entero para realizar la suma
	 */
	public Constructor(int nuevoValor)
	{
		this();
	 	id = getId() + nuevoValor;
	 	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
