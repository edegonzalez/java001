package deloitte.academy.sample;

import java.util.List;

public class ContenedorListas {
	public List<Boolean> listTrue;
	public List<Boolean> listFalse;
	
	
	
	public List<Boolean> getListTrue() {
		return listTrue;
	}
	public void setListTrue(List<Boolean> listTrue) {
		this.listTrue = listTrue;
	}
	public List<Boolean> getListFalse() {
		return listFalse;
	}
	public void setListFalse(List<Boolean> listFalse) {
		this.listFalse = listFalse;
	}


}
