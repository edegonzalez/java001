package deloitte.academy.sample;

/**
 * Una clase abstracta para Java es una clase de la que nunca se van a crear
 * instancias: simplemente va a servir como superclase a otras clases.
 * 
 * @author edegonzalez
 *
 */
public abstract class Factura {

	private int id;
	private double importe;
	
	 public Factura() {
		// TODO Auto-generated constructor stub
	}

	 
	public Factura(int id, double importe) {
		super();
		this.id = id;
		this.importe = importe;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public abstract double calcularTotal();
	 
	
}
