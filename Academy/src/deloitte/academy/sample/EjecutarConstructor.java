package deloitte.academy.sample;

public class EjecutarConstructor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Constructor constructor1 = new Constructor();
		
		System.out.println(constructor1.getId() + " " + constructor1.getNombre());
		
		
		Constructor constructor2 = new Constructor(2,"Jose Gonzalez");
		
		
		System.out.println(constructor2.getId() + " " + constructor2.getNombre());
		
		Constructor constructor3 = new Constructor(100);
		
		System.out.println("Suma: " + constructor3.getId());
	}

}
