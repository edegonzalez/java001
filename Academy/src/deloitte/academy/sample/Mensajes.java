package deloitte.academy.sample;

public enum Mensajes {
	OK("Ejecución exitosa.",200), Error("Ejecución con errores.",401);

	public String descripcion;
	public int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	private Mensajes() {
		// TODO Auto-generated constructor stub
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	private Mensajes(String descripcion, int codigo) {
		this.descripcion = descripcion;
		this.codigo=codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	
	
	
}
