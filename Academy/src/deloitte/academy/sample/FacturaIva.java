package deloitte.academy.sample;

public class FacturaIva extends Factura {
 
	@Override
	
	public double calcularTotal() {
		return this.getImporte() * 1.10;
	}
}
