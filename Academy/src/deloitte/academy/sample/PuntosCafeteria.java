package deloitte.academy.sample;

public class PuntosCafeteria {
	private int id;
	private String nombre;
	private String tipo;
	private int totalPuntos;

	public PuntosCafeteria() {
		// TODO Auto-generated constructor stub
	}

	
	public PuntosCafeteria(int id, String nombre, String tipo, int totalPuntos) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.totalPuntos = totalPuntos;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getTotalPuntos() {
		return totalPuntos;
	}

	public void setTotalPuntos(int totalPuntos) {
		this.totalPuntos = totalPuntos;
	}

	
}
