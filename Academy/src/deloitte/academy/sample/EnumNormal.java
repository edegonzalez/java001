package deloitte.academy.sample;


enum Color {
	ROJO, VERDE, AZUL;
}

/**
 * 
 * @author edegonzalez
 * Un simple ejemplo donde se declara enum
 * fuera de cualquier clase (Nota la palabra enum en lugar
 * de la palabra class)
 */

public class EnumNormal {

	/**
	 * El metodo principal.
	 * @param args
	 */
	public static void main(String[] args) {
		Color color = Color.VERDE;
		System.out.println(color);
	}

}
