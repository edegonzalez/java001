package deloitte.academy.sample;

/**
 * Java program to illustrate  
 * for-each loop 
 * @author edegonzalez
 *
 */
public class ForEach {

	public static void main(String[] arg) {
		{
			{
			boolean[] marks = { true, false, true, false, false };

			//boolean highest_marks = maximum(marks);
			//System.out.println("The highest score is " + highest_marks);
		}}
	}

	/**
	 * Method for each loop 
	 * @param numbers array type int.
	 * @return value type int.
	 */
	public static boolean maximum(boolean[] numbers) {
		//boolean maxSoFar = boolean[1];

		// for each loop
		for (boolean num : numbers) {
			//if (num > maxSoFar) {
				//maxSoFar = num;
			//}
		}
		return true;
	}

}
