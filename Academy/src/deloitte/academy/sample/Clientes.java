package deloitte.academy.sample;

public class Clientes extends Cafeteria implements RegistroPuntos {

	public Clientes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Clientes(int idVenta, double importe) {
		super(idVenta, importe);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		// TODO Auto-generated method stub
		double total = this.getImporte();
		return total;
	}

	@Override
	public String registrarPunto(int numVenta, String nombre) {
		// TODO Auto-generated method stub
		String mensaje = "";
		int contador = 0;

		for (PuntosCafeteria elemento : EjecutarCafeteria.puntosCafeteria) {
			if (elemento.getNombre().equals(nombre)) {
				contador += elemento.getTotalPuntos();
			}
		}

		if (contador == 0) {
			PuntosCafeteria objNuevo = new PuntosCafeteria(numVenta, nombre, "Cliente", 100);
			EjecutarCafeteria.puntosCafeteria.add(objNuevo);
		} else {
			for (PuntosCafeteria elemento : EjecutarCafeteria.puntosCafeteria) {
				if (elemento.getNombre().equals(nombre)) {
					elemento.setTotalPuntos(contador + 10);
				}
			}
		}

		return mensaje;
	}

}
