package deloitte.academy.sample;

/**
 * Overloading / Sobrecarga Overriding / Anulaci�n
 * 
 * El polimorfismo se aplica tanto a la anulaci�n (cuando una subclase cambia la
 * implementaci�n de un m�todo proporcionado por una superclase) como a la
 * sobrecarga (cuando una clase dada proporciona diferentes implementaciones
 * para el mismo m�todo, cambiando los tipos y / o el n�mero de par�metros).
 * Algunas de las diferentes formas de polimorfismo son est�ticas y din�micas.
 * El polimorfismo est�tico es cuando usamos la sobrecarga del m�todo, de modo
 * que el m�todo a llamar se define est�ticamente en tiempo de compilaci�n, en
 * funci�n de los par�metros en la llamada al m�todo. El polimorfismo din�mico
 * es cuando usamos la anulaci�n del m�todo, de modo que el m�todo a llamar se
 * define din�micamente en tiempo de ejecuci�n, en funci�n del tipo real del
 * objeto que llama al m�todo.
 * 
 * @author edegonzalez
 *
 */
public class Profesional {

	public int numSAP;
	public String nombre;

	/*
	 * ALT + SHIFT + S
	 */

	/**
	 * La sobrecarga de constructores es un concepto de tener m�s de un constructor
	 * con una lista de par�metros diferente, de tal manera que cada constructor
	 * realice una tarea diferente.
	 */
	public Profesional() {
		this.numSAP = 33392;
		this.nombre = "Eden Gonzalez";
	}

	public Profesional(int numSAP, String nombre) {
		super();
		this.numSAP = numSAP;
		this.nombre = nombre;
	}

	public Profesional(int numb) {
		this();
		numSAP = numSAP + numb;

	}

	public int getNumSAP() {
		return numSAP;
	}

	public void setNumSAP(int numSAP) {
		this.numSAP = numSAP;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/*
	 * Ctrl + space
	 */
	public static void main(String[] args) {

		// This object creation would call the default constructor
		Profesional profesional1 = new Profesional();
		System.out.println(profesional1.getNumSAP() + " " + profesional1.getNombre());

		// This object creation would call the parameterized
		Profesional profesional2 = new Profesional(33395, "Emilio");
		System.out.println(profesional2.getNumSAP() + " " + profesional2.getNombre());

		Profesional profesional3 = new Profesional(100);
		System.out.println(profesional3.getNumSAP());

	}

}
