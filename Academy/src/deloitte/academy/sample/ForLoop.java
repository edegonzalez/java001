package deloitte.academy.sample;

/**
 * Comentario a nivel clase
 * @author edegonzalez
 *
 */
public class ForLoop {

	/**
	 * Comentario para metodos
	 * @param args
	 */
	public static void main(String args[]) {
		
		/*
		 * Comentario simple
		 */
		for (int x = 2; x <= 4; x++) {
			System.out.println("Value of x:" + x);
		
		}
	}
}
