package deloitte.academy.sample;

import java.util.ArrayList;
import java.util.List;

public class ArrayListMatrix {
	 public static void main(String args[]){

	        List<ArrayList<String>> principal = new ArrayList<>();

	        ArrayList<String> nombre = new ArrayList<String>();
	        ArrayList<String> fecha = new ArrayList<String>();


	        nombre.add("Eden");
	        nombre.add("Jose");

	        fecha.add("04-03-2020");
	        fecha.add("05-04-2020");

	       

	        principal.add(nombre);
	        principal.add(fecha);

	       

	        for(ArrayList obj:principal){

	            ArrayList<String> temp = obj;

	            Integer indice = obj.indexOf(obj);
	            
	            obj.remove(indice);
	            
	            
	            for(String job : temp){
	                System.out.print(job+" ");
	            }
	            System.out.println();
	            
	            
	        }
	    }
}
