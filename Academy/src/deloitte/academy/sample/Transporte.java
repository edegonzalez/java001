package deloitte.academy.sample;

public enum Transporte {
	COCHE(60), CAMION(50), AVION(600), TREN(70), BARCO(20);
	
	private int velocidad;
	
	private Transporte() {
		// TODO Auto-generated constructor stub
	}


	private Transporte(int velocidad) {
		this.velocidad = velocidad;
	}


	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	
	 public static int getSuperVelocity(int d, int f){
         return (d * f);
   }
	
	


}
